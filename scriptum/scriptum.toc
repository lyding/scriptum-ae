\babel@toc {german}{}
\contentsline {section}{\numberline {1}Grundlagen}{1}{section.1}%
\contentsline {subsection}{\numberline {1.1}Ladung, Spannung und Strom}{1}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Kirchhoff Gesetze}{1}{subsection.1.2}%
\contentsline {subsubsection}{\numberline {1.2.1}Knotenpunktsatz}{1}{subsubsection.1.2.1}%
\contentsline {subsubsection}{\numberline {1.2.2}Maschengesetz}{2}{subsubsection.1.2.2}%
\contentsline {subsection}{\numberline {1.3}Reihenschaltung von Ohmschen Widerstaenden}{4}{subsection.1.3}%
\contentsline {subsection}{\numberline {1.4}Parallelschaltung Ohmscher Widerstaende}{6}{subsection.1.4}%
\contentsline {subsection}{\numberline {1.5}Spannungsteiler}{7}{subsection.1.5}%
\contentsline {subsubsection}{\numberline {1.5.1}Belasteter Spannungsteiler}{8}{subsubsection.1.5.1}%
\contentsline {subsection}{\numberline {1.6}Th\'{e}venin Theorem}{9}{subsection.1.6}%
\contentsline {subsubsection}{\numberline {1.6.1}Th\'{e}revin Aequivalant eines Spannungsteilers}{11}{subsubsection.1.6.1}%
\contentsline {section}{\numberline {2}Frequenzabhaengige Bauteile}{11}{section.2}%
\contentsline {subsection}{\numberline {2.1}Wechselspannung und Wechselstrom}{11}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Ohmscher Widerstand in Wechselstromkreises}{12}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}Kondenstor}{12}{subsection.2.3}%
\contentsline {subsubsection}{\numberline {2.3.1}Lade- und Entladeprozess}{13}{subsubsection.2.3.1}%
\contentsline {subsubsection}{\numberline {2.3.2}Wechselstromwiderstand des Kondensators}{16}{subsubsection.2.3.2}%
\contentsline {subsubsection}{\numberline {2.3.3}Komplexe Spannung und komplexer Strom}{16}{subsubsection.2.3.3}%
\contentsline {subsubsection}{\numberline {2.3.4}Kondensator als Gleichstromsperre}{17}{subsubsection.2.3.4}%
\contentsline {subsubsection}{\numberline {2.3.5}RC-Tiefpass}{17}{subsubsection.2.3.5}%
\contentsline {subsubsection}{\numberline {2.3.6}Tiefpass als Integrator}{20}{subsubsection.2.3.6}%
\contentsline {subsubsection}{\numberline {2.3.7}RC-Hochpass}{21}{subsubsection.2.3.7}%
\contentsline {subsubsection}{\numberline {2.3.8}Hochpass als Differenzierer}{23}{subsubsection.2.3.8}%
\contentsline {subsubsection}{\numberline {2.3.9}Tief- und Hochpaesse hoeherer Ordnung}{24}{subsubsection.2.3.9}%
\contentsline {subsubsection}{\numberline {2.3.10}Bandpass}{24}{subsubsection.2.3.10}%
\contentsline {subsubsection}{\numberline {2.3.11}Bandsperre}{25}{subsubsection.2.3.11}%
\contentsline {subsection}{\numberline {2.4}Induktivitaet}{26}{subsection.2.4}%
\contentsline {subsubsection}{\numberline {2.4.1}Einschalten einer Induktivitaet}{27}{subsubsection.2.4.1}%
\contentsline {subsubsection}{\numberline {2.4.2}Entladen einer Spule}{28}{subsubsection.2.4.2}%
\contentsline {subsubsection}{\numberline {2.4.3}Wechselstromwiderstand der Spule}{29}{subsubsection.2.4.3}%
\contentsline {subsubsection}{\numberline {2.4.4}RL-Hochpass}{30}{subsubsection.2.4.4}%
\contentsline {subsection}{\numberline {2.5}RL-Tiefpass}{31}{subsection.2.5}%
\contentsline {subsection}{\numberline {2.6}RLC-Schwingkreis}{32}{subsection.2.6}%
\contentsline {subsubsection}{\numberline {2.6.1}Reihenschwingkreis}{32}{subsubsection.2.6.1}%
\contentsline {subsubsection}{\numberline {2.6.2}Parallelschwingkreis}{38}{subsubsection.2.6.2}%
\contentsline {section}{\numberline {3}Nichtlineare Bauteile}{42}{section.3}%
\contentsline {subsection}{\numberline {3.1}Diode}{42}{subsection.3.1}%
\contentsline {subsubsection}{\numberline {3.1.1}Halbwellengleichrichter}{43}{subsubsection.3.1.1}%
\contentsline {subsubsection}{\numberline {3.1.2}Vollwellengleichrichter (Brueckengleichrichter)}{45}{subsubsection.3.1.2}%
\contentsline {subsubsection}{\numberline {3.1.3}DC-Spannungsmultiplizierer}{47}{subsubsection.3.1.3}%
\contentsline {subsubsection}{\numberline {3.1.4}Ueberspannungsschutz}{50}{subsubsection.3.1.4}%
\contentsline {subsubsection}{\numberline {3.1.5}Freilaufdiode}{51}{subsubsection.3.1.5}%
\contentsline {subsubsection}{\numberline {3.1.6}Zener-Diode}{51}{subsubsection.3.1.6}%
\contentsline {subsection}{\numberline {3.2}Physikalische Funktionsweise der Diode}{55}{subsection.3.2}%
\contentsline {section}{\numberline {4}Transistor}{56}{section.4}%
\contentsline {subsection}{\numberline {4.1}Bipolartransistor}{56}{subsection.4.1}%
\contentsline {subsubsection}{\numberline {4.1.1}Funktionsweise}{57}{subsubsection.4.1.1}%
\contentsline {subsection}{\numberline {4.2}Transistor als Schalter und Pulsgeneratorschaltungen}{58}{subsection.4.2}%
\contentsline {subsubsection}{\numberline {4.2.1}Treiben einer Last}{59}{subsubsection.4.2.1}%
\contentsline {subsubsection}{\numberline {4.2.2}Treiben einer geerdeten Last}{60}{subsubsection.4.2.2}%
\contentsline {subsection}{\numberline {4.3}Pulsgenerator I}{61}{subsection.4.3}%
\contentsline {subsection}{\numberline {4.4}Pulsgenerator II}{62}{subsection.4.4}%
\contentsline {subsection}{\numberline {4.5}Pulsgenerator III (Schmitt Trigger)}{64}{subsection.4.5}%
\contentsline {subsection}{\numberline {4.6}Stromverstaerker Modell}{65}{subsection.4.6}%
\contentsline {subsection}{\numberline {4.7}Emitterfolger}{65}{subsection.4.7}%
\contentsline {subsubsection}{\numberline {4.7.1}Emitterfolger als Spannungsregler}{69}{subsubsection.4.7.1}%
\contentsline {subsubsection}{\numberline {4.7.2}Emitterfolger fuer Wechselspannungen}{69}{subsubsection.4.7.2}%
\contentsline {subsection}{\numberline {4.8}Stromquelle}{70}{subsection.4.8}%
\contentsline {subsection}{\numberline {4.9}Verstaerker in Emitterschaltung}{71}{subsection.4.9}%
\contentsline {subsection}{\numberline {4.10}Ebers-Moll Modell des Transistors}{73}{subsection.4.10}%
\contentsline {subsection}{\numberline {4.11}Bedeutung des Ebers-Moll Modell fuer den Emitterfolger}{74}{subsection.4.11}%
\contentsline {subsection}{\numberline {4.12}Bedeutung des Ebers-Moll Modell fuer den Verstaerker in Emitterschaltung}{74}{subsection.4.12}%
\contentsline {subsection}{\numberline {4.13}Stabilisierung des Arbeitspunktes}{75}{subsection.4.13}%
\contentsline {subsubsection}{\numberline {4.13.1}Stromgegenkopplung}{75}{subsubsection.4.13.1}%
\contentsline {subsection}{\numberline {4.14}Gegenkopplung allgemein}{76}{subsection.4.14}%
\contentsline {subsection}{\numberline {4.15}Entwurf von Transistorschaltungen}{78}{subsection.4.15}%
\contentsline {subsubsection}{\numberline {4.15.1}Dimensionierung eines Emitterfolgers}{78}{subsubsection.4.15.1}%
\contentsline {subsubsection}{\numberline {4.15.2}Stromquelle}{80}{subsubsection.4.15.2}%
\contentsline {section}{\numberline {5}Operationsverstaerker}{81}{section.5}%
\contentsline {subsection}{\numberline {5.1}Invertierende Verstaerkerschaltung}{82}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}Nichtinvertierender Verstaerker}{83}{subsection.5.2}%
\contentsline {subsection}{\numberline {5.3}Aktive Filter}{85}{subsection.5.3}%
\contentsline {subsubsection}{\numberline {5.3.1}Aktiver Tiefpassfilter}{85}{subsubsection.5.3.1}%
\contentsline {subsubsection}{\numberline {5.3.2}Aktiver Hochpass}{90}{subsubsection.5.3.2}%
\contentsline {section}{Symbole}{94}{section*.95}%
