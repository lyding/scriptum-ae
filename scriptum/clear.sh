#!/bin/bash

rm *.aux
rm *.log
rm *.toc
rm *.glo
rm *.ist
rm *.pdf
rm *.sta
rm *.glg
rm *.gls
rm *.out
rm *.slo
rm *.lof
rm *.lot
