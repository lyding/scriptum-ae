"""
Generates the data to plot the transmission function,
the current as well as the voltage on the capacitor and inductance
of a rlc series oscillating circuit
"""

import numpy as np


def frac(a, b):
    return a / b


def h(w, r, L, c):
    return frac(1, 1 + (frac(w*L, r) - frac(1, w*r*c))**2)


def i(w, r, L, c):
    return frac(1, np.sqrt(r**2 + (w*L - frac(1, w*c))**2))


def uc(w, r, L, c):
    _is = i(w, r, L, c)
    return _is * frac(1, w*c)


def ul(w, r, L, c):
    _is = i(w, r, L, c)
    return _is * w*L


if __name__ == '__main__':
    r = 4
    L = 20e-6
    c = (1/20)*1e-6
    ws = np.linspace(1, 2e6, 100)
    _is = i(ws, r, L, c)
    ucs = uc(ws, r, L, c)
    uls = ul(ws, r, L, c)
    hs = h(ws, r, L, c)

    with open('rlc_series_data.csv', 'w') as fd:
        fd.write('omega, uc, ul, h\n')
        for i in range(len(ws)):
            fd.write(str(ws[i]) + ', ' +
                     str(ucs[i]) + ', ' +
                     str(uls[i]) + ', ' +
                     str(hs[i]) + '\n')
