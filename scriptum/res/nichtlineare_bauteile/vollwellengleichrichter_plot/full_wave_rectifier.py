
"""
Generate the data to plot the voltage on a full wave rectifier
before and after rectification
"""

import numpy as np
from copy import copy


def clamp(x, a, b):
    '''
    Clamp a value to an interval

    :param x: The value to clamp
    :param a: The lower bound
    :param b: The upper bound
    :returns: x clamped to [a, b]
    '''
    if x < a:
        return a
    if x > b:
        return b
    return x


def frac(a, b):
    return a / b


def f():
    '''
    Generate a timestamp array as well as a sin function
    and a clamped sin wave function on the timestamps
    '''
    ts = np.linspace(0, 1, 1000)
    ys = 5*np.sin(2*np.pi*ts * 4)
    rectified = copy(ys)
    rectified = np.abs(rectified)
    for i in range(len(rectified)):
        if rectified[i] < 0.7:
            rectified[i] = 0
    return ts, ys, rectified


if __name__ == '__main__':
    ts, ys, rectified = f()
    with open("full_wave_rectifier.csv", "w") as fd:
        fd.write("t, y, rectified\n")
        for i in range(len(ts)):
            fd.write(f'{ts[i]}, {ys[i]}, {rectified[i]}\n')
