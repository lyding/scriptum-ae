"""
Creates calculate the diode current for a diode voltage in the range
from zero to one volt.
"""

import numpy as np


def frac(a, b):
    return a / b


def i_diode(u_diode):
    i_s = 10e-12
    n = 1
    u_t = 25e-3
    return i_s * (np.exp(frac(u_diode, n*u_t)) - 1)


if __name__ == "__main__":
    _us = np.linspace(0, 1)
    _is = i_diode(_us)
    with open('diode_ui.csv', 'w') as fd:
        fd.write('u, i\n')
        for i in range(len(_us)):
            fd.write(f'{_us[i]}, {_is[i]}\n')
