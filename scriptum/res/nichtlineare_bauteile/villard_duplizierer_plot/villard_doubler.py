"""
Generates the data to plot the voltage over time diagram
on a villard doubler circuit
"""

import numpy as np


if __name__ == "__main__":
    ts = np.linspace(0, 1, 1000)
    ys = np.zeros(len(ts))
    out = np.zeros(len(ts))
    for i in range(len(ts)):
        x = ts[i]
        if x < .25 or (x > .5 and x < .75):
            ys[i] = 1
            out[i] = 2
        else:
            ys[i] = -1
            out[i] = 0
    with open("villard.csv", "w") as fd:
        fd.write("t, in, out\n")
        for i in range(len(ts)):
            fd.write(f'{ts[i]}, {ys[i]}, {out[i]}\n')
