'''
This module generates the data to reproduce the plot
fig. 2.18 on page 81 in the Arts of Electronics.
'''

import numpy as np

if __name__ == "__main__":
    time = np.linspace(0, 1, 1024)
    sin_data = np.sin(2*np.pi*time)
    sin_with_offset_data = sin_data - 0.05

    for i in range(len(sin_with_offset_data)):
        if sin_with_offset_data[i] < -0.5:
            sin_with_offset_data[i] = -0.5

    with open('data.csv', 'w') as fd:
        fd.write('t, y1, y2\n')
        for i in range(len(time)):
            fd.write(f'{time[i]}, {sin_data[i]}, {sin_with_offset_data[i]}\n')
