from random import random
from math import inf, sqrt

# The numer of anions and kations
N = 50
WIDTH = 2
HEIGHT = 2
RADIUS = 0.1


def d(coordA, coordB):
    return sqrt((coordA[0] - coordB[0])**2 + (coordA[1] - coordB[1])**2)


def get_minimum_distance(new_coord, coords):
    distance = inf
    for coord in coords:
        tmp_dist = d(coord, new_coord)
        if tmp_dist < distance:
            distance = tmp_dist
    return distance


if __name__ == '__main__':
    # Generate the anion coordinates
    anion_coords = []
    while len(anion_coords) < N:
        distance = 0
        while distance < 2 * RADIUS:
            x = RADIUS + random() * (WIDTH - 2*RADIUS)
            y = RADIUS + random() * (HEIGHT - 2*RADIUS)
            tmp_coord = (x, y)
            distance = get_minimum_distance(tmp_coord, anion_coords)
        anion_coords.append(tmp_coord)

    kation_coords = []
    while len(kation_coords) < N:
        distance = 0
        while distance < 2 * RADIUS:
            x = RADIUS + random() * (WIDTH - 2*RADIUS) + WIDTH
            y = RADIUS + random() * (HEIGHT - 2*RADIUS)
            tmp_coord = (x, y)
            distance = get_minimum_distance(tmp_coord, kation_coords)
        kation_coords.append(tmp_coord)

    with open('anions.dat', 'w') as fd:
        fd.write('x, y\n')
        for anion_coord in anion_coords:
            fd.write(str(anion_coord[0]) + ',' + str(anion_coord[1]) + '\n')

    with open('kations.dat', 'w') as fd:
        fd.write('x, y\n')
        for kation_coord in kation_coords:
            fd.write(str(kation_coord[0]) + ',' + str(kation_coord[1]) + '\n')

    # Generate the anion coordinates
    anion_coords = []
    while len(anion_coords) < N:
        distance = 0
        while distance < 2 * RADIUS:
            x = RADIUS + random() * (WIDTH - 4*RADIUS)
            y = RADIUS + random() * (HEIGHT - 2*RADIUS)
            tmp_coord = (x, y)
            distance = get_minimum_distance(tmp_coord, anion_coords)
        anion_coords.append(tmp_coord)

    kation_coords = []
    while len(kation_coords) < N:
        distance = 0
        while distance < 2 * RADIUS:
            x = 3*RADIUS + random() * (WIDTH - 4*RADIUS) + WIDTH
            y = RADIUS + random() * (HEIGHT - 2*RADIUS)
            tmp_coord = (x, y)
            distance = get_minimum_distance(tmp_coord, kation_coords)
        kation_coords.append(tmp_coord)

    with open('anions2.dat', 'w') as fd:
        fd.write('x, y\n')
        for anion_coord in anion_coords:
            fd.write(str(anion_coord[0]) + ',' + str(anion_coord[1]) + '\n')

    with open('kations2.dat', 'w') as fd:
        fd.write('x, y\n')
        for kation_coord in kation_coords:
            fd.write(str(kation_coord[0]) + ',' + str(kation_coord[1]) + '\n')
