from random import random
from math import inf, sqrt

# The numer of anions and kations
LOW_N = 20
HIGH_N = 65
WIDTH = 2
HEIGHT = 2
RADIUS = 0.1
XOFFSET = -1
YOFFSET = 0


def d(coordA, coordB):
    return sqrt((coordA[0] - coordB[0])**2 + (coordA[1] - coordB[1])**2)


def get_minimum_distance(new_coord, coords):
    distance = inf
    for coord in coords:
        tmp_dist = d(coord, new_coord)
        if tmp_dist < distance:
            distance = tmp_dist
    return distance


if __name__ == '__main__':
    # Generate the low density anion coordinates
    low_anion_coords = []
    while len(low_anion_coords) < LOW_N:
        distance = 0
        while distance < 2 * RADIUS:
            x = RADIUS + random() * (WIDTH - 2*RADIUS) + XOFFSET
            y = RADIUS + random() * (HEIGHT - 2*RADIUS) + YOFFSET
            tmp_coord = (x, y)
            distance = get_minimum_distance(tmp_coord, low_anion_coords)
        low_anion_coords.append(tmp_coord)

    high_anion_coords = []
    while len(high_anion_coords) < HIGH_N:
        distance = 0
        while distance < 2 * RADIUS:
            x = RADIUS + random() * (WIDTH - 2*RADIUS) + XOFFSET
            y = RADIUS + random() * (HEIGHT - 2*RADIUS) + HEIGHT + YOFFSET
            tmp_coord = (x, y)
            distance = get_minimum_distance(tmp_coord, high_anion_coords)
        high_anion_coords.append(tmp_coord)

    with open('low_anions.dat', 'w') as fd:
        fd.write('x, y\n')
        for anion_coord in low_anion_coords:
            fd.write(str(anion_coord[0]) + ',' + str(anion_coord[1]) + '\n')

    with open('high_anions.dat', 'w') as fd:
        fd.write('x, y\n')
        for anion_coord in high_anion_coords:
            fd.write(str(anion_coord[0]) + ',' + str(anion_coord[1]) + '\n')
