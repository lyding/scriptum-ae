                        % Schritt I       - Anlage und Definition des
                        % Vektors "t" mit 400 Werten im Intervall 0 bis 2s. 
t=linspace(0,2,400);
                        % Schritt II.     - Anlage und Definition der
                        % Kreisfrequenzvariablen, Anlage und Definition der
                        % Spannungsvektoren anhand von "w" und "t".
w1= 2*pi;
w2= 6*pi;
w3= 10*pi;
u1= sin(w1*t);
u2= sin(w2*t);
u3= sin(w3*t);
                        % Schritt III.    - Erstellen des mit Nullen
                        % besetzen Vektors "ua" f�r die Ausgangsspannung.
ua= zeros(1,400);
                        % Schritt IV.     - For-Schleife, die in 400
                        % Schritten der Funktion ua den jeweiligen neuen
                        % aus u1, u2, u3 resultierenden Spannungswert zuordnet
for i= (1:400)
    ua(i)= matlabopv(u1(i),u2(i),u3(i))
end;
                        % Schritt V.      - Erstellen des Plotfensters,
                        % plotten der Vektoren "u1", "u2", "u3", "ua" in
                        % ein betiteltes Diagramm im Bereich "t" (von 0-2),
                        % Einf�gen, Benennen und Ausrichten der Legende.
plot_window= figure;
u_plot= plot(t,ua, t,u1, t,u2, t,u3);
title('Rechteckfunktion aus Sinus Kurven');
xlabel('Zeit [s]');
ylabel('Spannung [V]');
 
legend('Ua','U1','U2','U3','Location','SouthEast');
