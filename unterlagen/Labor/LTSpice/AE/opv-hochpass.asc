Version 4
SHEET 1 880 680
WIRE 448 -80 208 -80
WIRE 448 0 448 -80
WIRE 240 64 112 64
WIRE 368 64 320 64
WIRE 208 160 208 -80
WIRE 112 176 112 64
WIRE 176 176 112 176
WIRE 336 192 240 192
WIRE 368 192 368 64
WIRE 368 192 336 192
WIRE 448 192 448 80
WIRE 560 192 448 192
WIRE -160 208 -176 208
WIRE 176 208 -160 208
WIRE 112 224 112 176
WIRE -176 240 -176 208
WIRE 448 256 448 192
WIRE 112 304 112 288
WIRE 560 368 560 192
WIRE -176 400 -176 320
WIRE 112 400 112 384
WIRE 208 448 208 224
WIRE 448 448 448 336
WIRE 448 448 208 448
FLAG 112 400 0
FLAG 560 368 0
FLAG -176 400 0
FLAG 336 192 OUT
FLAG -160 208 IN
SYMBOL Opamps\\opamp2 208 128 R0
SYMATTR InstName U1
SYMATTR Value LM741
SYMBOL voltage 448 240 R0
SYMATTR InstName V1
SYMATTR Value 12V
SYMBOL voltage 448 -16 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V2
SYMATTR Value 12V
SYMBOL voltage -176 224 R0
WINDOW 123 24 124 Left 2
WINDOW 39 0 0 Left 2
SYMATTR Value2 AC 1
SYMATTR InstName V3
SYMATTR Value SINE(0 1 1k)
SYMBOL Misc\\EuropeanResistor 336 48 R90
WINDOW 0 5 56 VBottom 2
WINDOW 3 27 56 VTop 2
SYMATTR InstName R2
SYMATTR Value 100k
SYMBOL Misc\\EuropeanResistor 96 288 R0
SYMATTR InstName R3
SYMATTR Value 10k
SYMBOL cap 96 224 R0
SYMATTR InstName C2
SYMATTR Value 10n
TEXT -40 520 Left 2 !.ac oct 10 1 10MEG
TEXT -40 480 Left 2 !.include "LM741.sub"
TEXT -40 560 Left 2 !.PARAM RIN 10k\n;.STEP PARAM RIN LIST 100 1k 10k
